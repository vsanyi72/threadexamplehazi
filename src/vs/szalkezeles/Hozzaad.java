package vs.szalkezeles;

import java.util.ArrayList;

public class Hozzaad extends Thread{
	

	private Termek termek;
	Kosar ks = new Kosar();

	public Hozzaad(Termek termek) {
		super();
		this.termek = termek;
	}


	@Override
	public void run() {
		
		ks.getKosar().add(this.termek);
	}


	public Kosar getKs() {
		return ks;
	}


	public void setKs(Kosar ks) {
		this.ks = ks;
	}


	public Termek getTermek() {
		return termek;
	}


	public void setTermek(Termek termek) {
		this.termek = termek;
	}


	
	

}
