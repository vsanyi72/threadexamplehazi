package vs.szalkezeles;

import java.util.ArrayList;

public class Kosar {

	private ArrayList<Termek> kosar = new ArrayList<>();

	public ArrayList<Termek> getKosar() {
		return kosar;
	}

	public void setKosar(ArrayList<Termek> kosar) {
		this.kosar = kosar;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		builder.append(kosar);
		return builder.toString();
	}  

	


}
