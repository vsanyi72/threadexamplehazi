package vs.szalkezeles;

public class Termek {
	public Termek(String megnevezes, double ar) {
		super();
		this.megnevezes = megnevezes;
		this.ar = ar;
	}
	private String megnevezes;
	private double ar;
	public String getMegnevezes() {
		return megnevezes;
	}
	public void setMegnevezes(String megnevezes) {
		this.megnevezes = megnevezes;
	}
	public double getAr() {
		return ar;
	}
	public void setAr(double ar) {
		this.ar = ar;
	}
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
			sb.append(this.megnevezes);
			sb.append(System.lineSeparator());
			sb.append(this.ar);
		return sb.toString();
	} 
	
	

}
